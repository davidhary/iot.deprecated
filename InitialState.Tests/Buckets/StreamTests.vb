﻿Imports Tek.InitialState
Namespace InitialStateClientTests
    ''' <summary> Enum extensions tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="3/14/2018" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class BucketTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub


        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " BUCKET CLASSES "

        ''' <summary> A telemetry. </summary>
        ''' <license>
        ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
        ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
        ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
        ''' </license>
        ''' <history date="5/4/2019" by="David" revision=""> Created. </history>
        Friend Class Telemetry
            Public Property Heading As Integer
            Public Property Speed As Double
            Public Property Status As String
        End Class

        ''' <summary> Reads access key. </summary>
        ''' <returns> The access key. </returns>
        Friend Shared Function ReadAccessKey() As String
            Return My.Computer.FileSystem.ReadAllText(My.Settings.AccessKeyFileName)
        End Function

#End Region

#Region " BUCKET TESTS "

        ''' <summary> Handles the exception event. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Error event information. </param>
        Private Sub HandleExceptionEvent(ByVal sender As Object, ByVal e As IO.ErrorEventArgs)
            Assert.IsNotNull(e, $"{e.GetException.ToString}")
        End Sub

        ''' <summary> (Unit Test Method) tests stream telemetry event. </summary>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <remarks> Pass 5/7/2019 </remarks>
        <TestMethod>
        Public Sub StreamTelemetryEventTest()
            Dim accessKey As String = BucketTests.ReadAccessKey
            Try
                ' setup a configuration and read the access key from the private file.
                Dim config As New Tek.InitialState.InitialStateConfig() With {.AccessKey = BucketTests.ReadAccessKey}
                Dim bucketKey As String = "isr_Telemetry"

                ' instantiate an initial state event client and inject a configuration 
                Dim eventClient As New Tek.InitialState.EventClient(config)

                AddHandler eventClient.ExceptionOccurred, AddressOf Me.HandleExceptionEvent

                ' create a bucket
                eventClient.CreateBucket(bucketKey, "my telemetry bucket")

                Dim rnd As New Random(DateTime.Now.Second)

                ' create a telemetry event
                Dim telemetry As New Telemetry() With {.Heading = 1 + CInt(360 * rnd.NextDouble),
                                                       .Speed = 30 + 10 * rnd.NextDouble, .Status = "normal"}

                ' Send the event(s)
                eventClient.SendEvents(Of Telemetry)(telemetry, bucketKey, sendAsync:=False)

                Assert.AreEqual(My.Settings.ExpectedRateLimit, eventClient.RateLimit.Limit, "expects a specific rate limit")

            Catch ex As InitialStateException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try

        End Sub

        ''' <summary> (Unit Test Method) tests versions. </summary>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        <TestMethod>
        Public Sub VersionsTest()
            Dim accessKey As String = BucketTests.ReadAccessKey
            Try
                ' setup a configuration and read the access key from the private file.
                Dim config As New Tek.InitialState.InitialStateConfig() With {.AccessKey = BucketTests.ReadAccessKey,
                                                                              .DefaultBucketKey = My.Settings.DefaultBucketKey}

                ' instantiate an initial state event client and inject a configuration 
                Dim eventClient As New Tek.InitialState.EventClient(config)

                AddHandler eventClient.ExceptionOccurred, AddressOf Me.HandleExceptionEvent

                Dim actualVersion As Version = eventClient.RequestLastVersion()
                Dim expectedVersion As Version = New Version(My.Settings.ExpectedVersion)
                Assert.IsTrue(expectedVersion.Equals(actualVersion), $"expected version {expectedVersion} should equal actual version {actualVersion}")

            Catch ex As InitialStateException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try

        End Sub

#End Region

    End Class
End Namespace