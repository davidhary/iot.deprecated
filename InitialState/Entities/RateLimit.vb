﻿Imports RestSharp
''' <summary> A rate limit. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/8/2019" by="David" revision=""> Created. </history>
Public Class RateLimit

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="response"> The response. </param>
    Friend Sub New(ByVal response As IRestResponse)
        MyBase.New()
        Me.Parse(response)
    End Sub

    ''' <summary> Parses the given response. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="response"> The response. </param>
    Private Sub Parse(ByVal response As IRestResponse)
        If response Is Nothing Then Throw New ArgumentNullException(NameOf(response))
        Dim l As List(Of Parameter) = response.Headers.ToList
        Me.Limit = Integer.Parse(l.Find(Function(x) x.Name = RateLimit.LimitHeader).Value.ToString)
        Me.Remaining = Integer.Parse(l.Find(Function(x) x.Name = RateLimit.RemainingHeader).Value.ToString)
        Me.Reset = Integer.Parse(l.Find(Function(x) x.Name = RateLimit.ResetHeader).Value.ToString)
        If response.StatusCode = RateLimit.RateLimitedRequestStatusCode Then
            Me.RetryAfter = Integer.Parse(l.Find(Function(x) x.Name = RateLimit.RetryAfterHeader).Value.ToString)
        End If
    End Sub

    Private Const LimitHeader As String = "X-RateLimit-Limit"
    Private Const RemainingHeader As String = "X-RateLimit-Remaining"
    Private Const ResetHeader As String = "X-RateLimit-Reset"
    Private Const RetryAfterHeader As String = "Retry-After"
    ''' <summary> The rate limited request status code. </summary>
    Public Const RateLimitedRequestStatusCode As Integer = 429 ' too many requests

    ''' <summary> Gets or sets the rate limit in requests per 10 second interval. </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Limit</c><para>
    ''' This Is the current number of requests per 10 second interval. This value is based on the
    ''' account plan and accessKey rights</para>
    ''' </remarks>
    ''' <value> The rate limit in requests per 10 second interval. </value>
    Public Property Limit As Integer

    ''' <summary>
    ''' Gets or sets the number of requests left from the <see cref="Limit"/> during a 10 second
    ''' interval.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Remaining</c><para>
    ''' The number of requests left from the <see cref="Limit"/> during a 10 second interval</para>
    ''' </remarks>
    ''' <value>
    ''' The number of requests left from the <see cref="Limit"/> during a 10 second interval.
    ''' </value>
    Public Property Remaining As Integer

    ''' <summary>
    ''' Gets or sets the Unix epoch in seconds indicating when the <see cref="Remaining"/> will be
    ''' reset back to the value of <see cref="Limit"/>.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Reset</c><para>
    ''' The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
    ''' the value of <see cref="Limit"/></para>
    ''' </remarks>
    ''' <value>
    ''' The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
    ''' the value of <see cref="Limit"/>.
    ''' </value>
    Public Property Reset As Long

    Public ReadOnly Property ResetTime As DateTime
        Get
            Return EventClient.EpochStartTime.AddSeconds(Me.Reset)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the number of seconds requesting code should wait before retrying the request.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>Retry-After</c><para>
    ''' This number of seconds requesting code should wait before retrying the request</para><para>
    ''' This header Is only returned during a rate limited request represented by an HTTP 429 Status Code. 
    ''' </para>
    ''' </remarks>
    ''' <value>
    ''' This number of seconds requesting code should wait before retrying the request.
    ''' </value>
    Public Property RetryAfter As Long?

    ''' <summary> Gets the retry after timespan. </summary>
    ''' <value> The retry after timespan. </value>
    Public ReadOnly Property RetryAfterTimespan As TimeSpan
        Get
            Return If(Me.RetryAfter.HasValue, TimeSpan.FromSeconds(Me.RetryAfter.Value), TimeSpan.Zero)
        End Get
    End Property

    ''' <summary> Gets the retry after time. </summary>
    ''' <value> The retry after time. </value>
    Public ReadOnly Property RetryAfterTime As DateTime
        Get
            Return If(Me.RetryAfter.HasValue, EventClient.EpochStartTime.AddSeconds(Me.RetryAfter.Value), EventClient.EpochStartTime)
        End Get
    End Property

End Class