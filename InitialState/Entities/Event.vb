﻿''' <summary> An event. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/4/2019" by="David" revision=""> Created. </history>
Friend Class [Event]

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the key. </summary>
    ''' <value> The key. </value>
    Public Property key() As String
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property value() As String
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the epoch. </summary>
    ''' <value> The epoch. </value>
    Public Property epoch() As Double
#Enable Warning IDE1006 ' Naming Styles

End Class
