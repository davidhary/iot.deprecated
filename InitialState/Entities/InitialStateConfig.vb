Imports System.Configuration
''' <summary> An initial state configuration. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/4/2019" by="David" revision=""> Created. </history>
Public Class InitialStateConfig
    Inherits ConfigurationSection

    ''' <summary> Gets or sets the access key. </summary>
    ''' <value> The access key. </value>
    <ConfigurationProperty("accessKey", IsRequired:=True)>
    Public Property AccessKey() As String
        Get
            Return CStr(Me("accessKey"))
        End Get
        Set(ByVal value As String)
            Me("accessKey") = value
        End Set
    End Property

    ''' <summary> Gets or sets the API base. </summary>
    ''' <value> The API base. </value>
    <ConfigurationProperty("apiBase", DefaultValue:="https://groker.init.st/api")>
    Public Property ApiBase() As String
        Get
            Return CStr(Me("apiBase"))
        End Get
        Set(ByVal value As String)
            Me("apiBase") = value
        End Set
    End Property

    ''' <summary> Gets or sets the API version. </summary>
    ''' <value> The API version. </value>
    <ConfigurationProperty("apiVersion", DefaultValue:="~0")>
    Public Property ApiVersion() As String
        Get
            Return CStr(Me("apiVersion"))
        End Get
        Set(ByVal value As String)
            Me("apiVersion") = value
        End Set
    End Property

    ''' <summary> Gets or sets the default bucket key. </summary>
    ''' <value> The default bucket key. </value>
    <ConfigurationProperty("defaultBucketKey")>
    Public Property DefaultBucketKey() As String
        Get
            Return CStr(Me("defaultBucketKey"))
        End Get
        Set(ByVal value As String)
            Me("defaultBucketKey") = value
        End Set
    End Property
End Class

