﻿Imports System.Net
''' <summary> Exception for signaling initial state errors. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/4/2019" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")>
Public Class InitialStateException
    Inherits Exception

    ''' <summary> Constructor. </summary>
    ''' <param name="message">         The message. </param>
    ''' <param name="statusCode">      The status code. </param>
    ''' <param name="requestBody">     The request body. </param>
    ''' <param name="responseContent"> The response content. </param>
    Public Sub New(ByVal message As String, ByVal statusCode As HttpStatusCode, ByVal requestBody As String, ByVal responseContent As String)
        MyBase.New(message)
        Me.StatusCode = statusCode
        Me.RequestBody = requestBody
        Me.ResponseContent = responseContent
    End Sub

    Private Shared Function ValidatedResponse(ByVal response As RestSharp.IRestResponse) As RestSharp.IRestResponse
        If response Is Nothing Then Throw New ArgumentNullException(NameOf(response))
        Return response
    End Function

    Private Shared Function ValidatedRequest(ByVal request As RestSharp.IRestRequest) As RestSharp.IRestRequest
        If request Is Nothing Then Throw New ArgumentNullException(NameOf(request))
        Return request
    End Function

    ''' <summary> Builds response body. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="request"> The request. </param>
    ''' <returns> A String. </returns>
    Private Shared Function BuildResponseBody(ByVal request As RestSharp.IRestRequest) As String
        If request Is Nothing Then Throw New ArgumentNullException(NameOf(request))
        Dim body As String = "<empty>"
        Dim bodyRequest As RestSharp.Parameter = request.Parameters.FirstOrDefault(Function(x) x.Type = RestSharp.ParameterType.RequestBody)
        If bodyRequest IsNot Nothing Then body = bodyRequest.Value.ToString()
        Return body
    End Function

    ''' <summary> Constructor. </summary>
    ''' <param name="message">  The message. </param>
    ''' <param name="response"> The response. </param>
    ''' <param name="request">  The request. </param>
    Public Sub New(ByVal message As String, ByVal response As RestSharp.IRestResponse, ByVal request As RestSharp.IRestRequest)
        Me.New(message, InitialStateException.ValidatedResponse(response).ErrorException, response.StatusCode,
               InitialStateException.BuildResponseBody(request), response.Content)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="statusCode">      The status code. </param>
    ''' <param name="requestBody">     The request body. </param>
    ''' <param name="responseContent"> The response content. </param>
    Public Sub New(ByVal statusCode As HttpStatusCode, ByVal requestBody As String, ByVal responseContent As String)
        MyBase.New()
        Me.StatusCode = statusCode
        Me.RequestBody = requestBody
        Me.ResponseContent = responseContent
    End Sub

    Public Sub New(ByVal message As String, ByVal exception As Exception, ByVal statusCode As HttpStatusCode, ByVal requestBody As String, ByVal responseContent As String)
        MyBase.New(message, exception)
        Me.StatusCode = statusCode
        Me.RequestBody = requestBody
        Me.ResponseContent = responseContent
    End Sub

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode() As HttpStatusCode

    ''' <summary> Gets or sets the request body. </summary>
    ''' <value> The request body. </value>
    Public Property RequestBody() As String

    ''' <summary> Gets or sets the response content. </summary>
    ''' <value> The response content. </value>
    Public Property ResponseContent() As String
End Class

