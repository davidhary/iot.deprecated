﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = &H2100 'isr.Core.Agnostic.My.ProjectTraceEventId.InitialStateClient

        Public Const AssemblyTitle As String = "Initial State Client Library"
        Public Const AssemblyDescription As String = "Initial State Client Library"
        Public Const AssemblyProduct As String = "Initial.State.Client.2019"

    End Class

End Namespace

