' General Information about an assembly is controlled through the following 
Imports System.Reflection

' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyCompany("Tektronix, Inc.")>
<Assembly: AssemblyCopyright("(c) 2019 Tektronix, Inc. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")>
<Assembly: AssemblyVersion("1.0.*")>

Namespace My
    ''' <summary> Information about this and related projects in this solution. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module SolutionInfo
        Public Const PublicKey As String = "0024000004800000940000000602000000240000525341310004000001000100dba29c0e6dba927c84e79ce36609bb871da7c63c8ef88addafa51ccc967604e03c21afeb0cb5f5e74028e1f151bf92e9fe73166bfab3a61c4db81e8dad98c020f06368cce665735e9a2e42741b80ea60a3dcc6b4ad212928baf132b99c0ce9a2d0eae0b14d4fb2b7d2e4d5eba898b61e1e69c1d47db03a92eb9324137084bffb"
    End Module
End Namespace