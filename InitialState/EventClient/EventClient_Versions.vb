﻿'
' Versions Requests
' 
Imports RestSharp
Imports RestSharp.Serialization.Json

Partial Public Class EventClient

    ''' <summary> Request versions response. </summary>
    ''' <exception cref="InitialStateException"> Thrown when an Initial State error condition occurs. </exception>
    ''' <returns> An IRestResponse. </returns>
    Private Function RequestVersionsResponse() As IRestResponse
        Dim activity As String = String.Empty
        Dim request As New RestRequest("/versions", Method.GET)
        request.JsonSerializer = New JsonSerializer()
        activity = "requesting versions"
        Dim response As IRestResponse = Me.RestClient.Execute(request)
        If Me.IsFailed(response) Then
            Throw New InitialStateException($"{response.StatusCode} {activity} from {response.ResponseUri}", response, request)
        End If
        Return response
    End Function

    ''' <summary> Request last version. </summary>
    ''' <returns> A Version. </returns>
    Public Function RequestLastVersion() As Version
        Dim result As New Version()
        Dim response As IRestResponse = Me.RequestVersionsResponse()
        Dim ser As New JsonSerializer
        Dim l As List(Of String) = ser.Deserialize(Of List(Of String))(response)
        result = New Version(l.Last)
        Return result
    End Function

End Class

