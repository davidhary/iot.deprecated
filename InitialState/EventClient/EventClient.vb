﻿Imports System.Configuration
Imports RestSharp
Imports RestSharp.Serialization.Json

''' <summary> An initial state events client. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/6/2019" by="David" revision=""> Created. </history>
Public Class EventClient
    Implements IEventClient

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary>
    ''' Creates an API Client for creating Initial State Buckets and sending events to the bucket.
    ''' </summary>
    ''' <param name="config">Optional configuration injection in lieu of using external file configuration </param>
    ''' <exception cref="ConfigurationException">Thrown when there isn't an Access Key in the injected configuration or the discovered file configuration.</exception>
    Public Sub New()
        Me.New(DirectCast(ConfigurationManager.GetSection(EventClient.InitialStateSectionName), InitialStateConfig))
    End Sub

    ''' <summary>
    ''' Creates an API Client for creating Initial State Buckets and sending events to the bucket.
    ''' </summary>
    ''' <exception cref="ConfigurationException"> Thrown when a Configuration error condition occurs. </exception>
    ''' <param name="config"> The configuration. </param>
    Public Sub New(ByVal config As InitialStateConfig)
        MyBase.New()
        Me._Version = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion
        Me._Config = config
        Me._DefaultBucketKey = config.DefaultBucketKey
        If String.IsNullOrEmpty(config.AccessKey) Then
            Throw New ConfigurationException("access key is required")
        End If
        Me._RestClient = New RestClient(config.ApiBase) With {.UserAgent = $"{UserAgentBase}/{Me._Version}"}
    End Sub

#End Region

#Region " INITIAL STATE "

    Public Const InitialStateSectionName As String = "initialstate"
    Public Const UserAgentBase As String = "initialstate_net"

#End Region

#Region " REST CLIENT "

    ''' <summary> Gets or sets the configuration. </summary>
    ''' <value> The configuration. </value>
    Protected ReadOnly Property Config As InitialStateConfig

    ''' <summary> Gets or sets the version. </summary>
    ''' <value> The version. </value>
    Protected ReadOnly Property Version As String

    ''' <summary> Gets or sets the epoch start time. </summary>
    ''' <value> The epoch start date. </value>
    Public Shared ReadOnly Property EpochStartTime As New DateTime(1970, 1, 1)

    ''' <summary> Gets or sets the REST client. </summary>
    ''' <value> The REST client. </value>
    Protected ReadOnly Property RestClient As RestClient

    ''' <summary> Gets or sets the default bucket key. </summary>
    ''' <value> The default bucket key. </value>
    Protected ReadOnly Property DefaultBucketKey As String

    ''' <summary> Query if 'response' failed. </summary>
    ''' <param name="response"> The response. </param>
    ''' <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
    Private Function IsFailed(ByVal response As IRestResponse) As Boolean
        Return IsFailed(CInt(Math.Truncate(response.StatusCode)))
    End Function

    ''' <summary> Query if 'response' failed. </summary>
    ''' <param name="statusCode"> The status code. </param>
    ''' <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
    Private Function IsFailed(ByVal statusCode As Integer) As Boolean
        Return statusCode > 299 OrElse statusCode < 200
    End Function

#End Region

#Region " BUCKET "

    ''' <summary>
    ''' Create a bucket to associate events with. This is an idempotent action unless a bucket has
    ''' been deleted.
    ''' </summary>
    ''' <exception cref="InitialStateException"> If the API responds with a non-successful HTTP Status
    '''                                          Code, this exception is thrown. </exception>
    ''' <param name="key">  (Optional) Bucket Key, should be unique in the scope of an Access Key and
    '''                     is generated by a caller; if not provided, a bucket key is generated and
    '''                     associated with the instantiation. </param>
    ''' <param name="name"> (Optional) Optional name of the bucket. If no name is provided, the
    '''                     bucket key is used. </param>
    ''' <param name="tags"> (Optional) Optional array of string tags to attach to the bucket upon
    '''                     creation. These do not get updated on subsequent creations of the same
    '''                     Bucket Key/Access Key. </param>
    ''' <returns> The Bucket Key for this bucket. </returns>
    Public Function CreateBucket(Optional ByVal key As String = Nothing, Optional ByVal name As String = Nothing,
                                 Optional ByVal tags() As String = Nothing) As String Implements IEventClient.CreateBucket

        If String.IsNullOrEmpty(key) Then key = Me.DefaultBucketKey
        If String.IsNullOrEmpty(key) Then key = Guid.NewGuid().ToString("N") : Me._DefaultBucketKey = key
        If String.IsNullOrEmpty(name) Then name = key

        Dim request As New RestRequest("/buckets", Method.POST)
        request.AddHeader("X-IS-AccessKey", Me.Config.AccessKey)
        request.AddHeader("Content-Type", "application/json")
        request.AddHeader("Accept-Version", Me.Config.ApiVersion)
        request.JsonSerializer = New JsonSerializer()

        request.AddJsonBody(New With {
                Key .bucketName = name,
                Key .bucketKey = key,
                Key tags
            })

        Dim response As IRestResponse = Me.RestClient.Execute(request)

        If CInt(Math.Truncate(response.StatusCode)) > 299 OrElse CInt(Math.Truncate(response.StatusCode)) < 200 Then
            Throw New InitialStateException($"{response.StatusCode} creating bucket {response.ResponseUri} ({Me.AccessBucket(key)})", response, request)
        End If

        Return key
    End Function

    ''' <summary> Gets an epoch. </summary>
    ''' <param name="timestamp?"> optional timestamp override, if this isn't provided, one is
    '''                           automatically generated from the system clock. </param>
    ''' <returns> The epoch. </returns>
    Private Function GetEpoch(ByVal timestamp? As DateTime) As Double
        If timestamp Is Nothing Then timestamp = DateTime.UtcNow
        Return timestamp.Value.Subtract(EventClient.EpochStartTime).TotalMilliseconds / 1000
    End Function

#End Region

#Region " EXCEPTION HANDLERS "

    ''' <summary> The exception occurred. </summary>
    Public Event ExceptionOccurred As EventHandler(Of IO.ErrorEventArgs)

    ''' <summary> Raises the error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnExceptionOccurred(ByVal e As System.IO.ErrorEventArgs)
        Dim evt As EventHandler(Of IO.ErrorEventArgs) = Me.ExceptionOccurredEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the error event. </summary>
    ''' <param name="exception"> The exception. </param>
    Protected Sub OnExceptionOccurred(ByVal exception As Exception)
        Me.OnExceptionOccurred(New IO.ErrorEventArgs(exception))
    End Sub

#End Region

End Class
