﻿''' <summary> Interface for initial state events client. </summary>
''' <license>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface IEventClient

    ''' <summary> Creates a bucket. </summary>
    ''' <param name="key">  (Optional) The key. </param>
    ''' <param name="name"> (Optional) The name. </param>
    ''' <param name="tags"> (Optional) The tags. </param>
    ''' <returns> The new bucket. </returns>
    Function CreateBucket(Optional ByVal key As String = Nothing, Optional ByVal name As String = Nothing, Optional ByVal tags() As String = Nothing) As String

    ''' <summary> Sends an event. </summary>
    ''' <param name="key">        The key. </param>
    ''' <param name="value">      The value. </param>
    ''' <param name="bucketKey">  (Optional) The bucket key. </param>
    ''' <param name="timestamp?"> (Optional) The timestamp? </param>
    ''' <param name="sendAsync">  (Optional) True to send asynchronous. </param>
    Sub SendEvent(ByVal key As String, ByVal value As String, Optional ByVal bucketKey As String = Nothing, Optional ByVal timestamp? As Date = Nothing, Optional ByVal sendAsync As Boolean = True)

    ''' <summary> Sends the events. </summary>
    ''' <param name="obj">        The object. </param>
    ''' <param name="bucketKey">  (Optional) The bucket key. </param>
    ''' <param name="timestamp?"> (Optional) The timestamp? </param>
    ''' <param name="sendAsync">  (Optional) True to send asynchronous. </param>
    Sub SendEvents(Of T)(ByVal obj As T, Optional ByVal bucketKey As String = Nothing, Optional ByVal timestamp? As Date = Nothing, Optional ByVal sendAsync As Boolean = True)

End Interface
