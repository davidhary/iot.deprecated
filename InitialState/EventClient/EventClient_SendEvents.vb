﻿'
' Send Events
' 
Imports System.Reflection
Imports RestSharp
Imports RestSharp.Serialization.Json

Partial Public Class EventClient
    Implements IEventClient

    ''' <summary> A simple method to immediately send a single event. </summary>
    ''' <exception cref="InitialStateException">  If the API responds with a non-successful HTTP Status
    '''                                           Code, this exception is thrown. </exception>
    ''' <exception cref="ConfigurationException"> Thrown if there is no bucket key provided as a
    '''                                           parameter, in the constructor configuration or in
    '''                                           the configuration file. </exception>
    ''' <param name="key">       key of the event to associate the value to. </param>
    ''' <param name="value">     value of the event. </param>
    ''' <param name="bucketKey"> (Optional) optional override of the bucket key associated with this
    '''                          instantiation. </param>
    ''' <param name="timestamp"> (Optional) optional timestamp override, if this isn't provided, one
    '''                          is automatically generated from the system clock. </param>
    ''' <param name="sendAsync"> (Optional) optional send event without waiting for confirmed response
    '''                          or wait for confirmed response. </param>
    Public Sub SendEvent(ByVal key As String, ByVal value As String, Optional ByVal bucketKey As String = Nothing,
                             Optional ByVal timestamp As DateTime? = Nothing, Optional ByVal sendAsync As Boolean = True) Implements IEventClient.SendEvent
        Dim dict As IDictionary(Of String, String) = New Dictionary(Of String, String) From {{key, value}}
        Me.SendEvents(dict, bucketKey, timestamp)
    End Sub

    ''' <summary> Primary method for sending events to a Bucket in Initial State. </summary>
    ''' <exception cref="ConfigurationException"> Thrown if there is no bucket key provided as a
    '''                                           parameter, in the constructor configuration or in
    '''                                           the configuration file. </exception>
    ''' <exception cref="InitialStateException">  If the API responds with a non-successful HTTP Status
    '''                                           Code, this exception is thrown. </exception>
    ''' <param name="obj">        object containing keys and values to send to an initial state bucket. </param>
    ''' <param name="bucketKey">  (Optional) optional override of the bucket key associated with this
    '''                           instantiation. </param>
    ''' <param name="timestamp"> optional timestamp override, if this isn't provided, one is
    '''                              automatically generated from the system clock. </param>
    ''' <param name="sendAsync">  (Optional) optional send event without waiting for confirmed response
    '''                           or wait for confirmed response. </param>
    '''
    '''<typeparam name="T"> Type of object to parse; expected to be a key-value type object. </typeparam>
    Public Sub SendEvents(Of T)(ByVal obj As T, Optional ByVal bucketKey As String = Nothing,
                                Optional ByVal timestamp As DateTime? = Nothing,
                                Optional ByVal sendAsync As Boolean = True) Implements IEventClient.SendEvents
        Dim epoch As Double = GetEpoch(timestamp)
        Dim events As IEnumerable(Of [Event]) = Nothing ' List(Of [Event]) = Nothing
        If GetType(T).IsAssignableFrom(GetType(IDictionary(Of String, String))) Then
            events = DirectCast(obj, IDictionary(Of String, String)).Select(Function(kvp) New [Event] With {
                .epoch = epoch,
                .key = kvp.Key,
                .value = kvp.Value
            }) ' .ToList()
        Else
            Dim properties() As PropertyInfo = GetType(T).GetProperties()
            events = properties.Select(Function(prop) New [Event] With {
                .epoch = epoch,
                .key = prop.Name,
                .value = prop.GetValue(obj).ToString()
            }) ' .ToList()
        End If
        Me.SendEvents(events, bucketKey, sendAsync)
    End Sub

    ''' <summary> Called by the Primary method for sending events to a Bucket in Initial State. </summary>
    ''' <exception cref="InitialStateException">  If the API responds with a non-successful HTTP Status
    '''                                           Code, this exception is thrown. </exception>
    ''' <exception cref="ConfigurationException"> Thrown if there is no bucket key provided as a
    '''                                           parameter, in the constructor configuration or in
    '''                                           the file configuration. </exception>
    ''' <param name="events">    The events. </param>
    ''' <param name="bucketKey"> (Optional) optional override of the bucket key associated with this
    '''                          instantiation. </param>
    ''' <param name="sendAsync"> (Optional) optional send event without waiting for confirmed response
    '''                          or wait for confirmed response. </param>
    '''
    Friend Sub SendEvents(ByVal events As IEnumerable(Of [Event]), Optional ByVal bucketKey As String = Nothing, Optional ByVal sendAsync As Boolean = True)

        Dim activity As String = String.Empty
        If String.IsNullOrEmpty(bucketKey) Then bucketKey = Me.Config.DefaultBucketKey
        If String.IsNullOrEmpty(bucketKey) Then Throw New ConfigurationException("bucket key is required")
        Dim request As New RestRequest("/events", Method.POST)
        request.AddHeader("X-IS-AccessKey", Me.Config.AccessKey)
        request.AddHeader("X-IS-BucketKey", bucketKey)
        request.AddHeader("Accept-Version", Me.Config.ApiVersion)
        request.JsonSerializer = New JsonSerializer()
        request.AddJsonBody(events)
        activity = "submitting events"
        If sendAsync Then
            Me.RestClient.ExecuteAsync(request, Sub(response)
                                                    If Me.IsFailed(response) Then
                                                        Me.OnExceptionOccurred(New InitialStateException($"{response.StatusCode} {activity} to {response.ResponseUri} ({Me.AccessBucket(bucketKey)})",
                                                                                    response, request))
                                                    Else
                                                        Me.OnSendEventCompleted(response)
                                                    End If
                                                End Sub)
        Else
            Dim response As IRestResponse = Me.RestClient.Execute(request)
            If Me.IsFailed(response) Then
                Throw New InitialStateException($"{response.StatusCode} {activity} to {response.ResponseUri} ({Me.AccessBucket(bucketKey)})",
                                                response, request)
            Else
                Me.OnSendEventCompleted(response)
            End If
        End If
    End Sub

    ''' <summary> Access bucket. </summary>
    ''' <param name="bucketKey"> the bucket key associated with this instantiation. </param>
    ''' <returns> A String. </returns>
    Private Function AccessBucket(ByVal bucketKey As String) As String
        Return $"{Me.Config.AccessKey.Substring(0, 4)}...{Me.Config.AccessKey.Substring(Me.Config.AccessKey.Length - 5, 4)}:{bucketKey}"
    End Function

    ''' <summary> Gets or sets the rate limit. </summary>
    ''' <value> The rate limit. </value>
    Public Overridable Property RateLimit As RateLimit

#Region " SEND COMPLETED "

    ''' <summary> Event queue for all listeners interested in <see cref="SendCompleted"/> events. </summary>
    Public Event SendCompleted As EventHandler(Of System.EventArgs)

    ''' <summary> Raises the <see cref="SendCompleted"/> event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnSendCompleted(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.SendCompletedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Executes the send event completed action. </summary>
    ''' <param name="response"> The response. </param>
    Private Sub OnSendEventCompleted(ByVal response As IRestResponse)
        Me.RateLimit = New RateLimit(response)
        Me.OnSendCompleted(System.EventArgs.Empty)
    End Sub

#End Region

End Class

