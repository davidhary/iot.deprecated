﻿using System;
using System.Net;

namespace InitialStateClient
{ 
	public class InitialStateException : Exception
	{
		public InitialStateException(string message, HttpStatusCode statusCode,
                                     string requestBody, string responseContent) : base(message)
		{
            
			StatusCode = statusCode;
			RequestBody = requestBody;
			ResponseContent = responseContent;
		}

		public HttpStatusCode StatusCode { get; set; }
		public string RequestBody { get; set; }
		public string ResponseContent { get; set; }
	}

	public class ConfigurationException : Exception
	{
		public ConfigurationException(string message) : base(message)
		{
			
		}
	}
}