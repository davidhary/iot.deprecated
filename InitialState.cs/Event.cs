﻿namespace InitialStateClient
{
	class Event
	{
		public string key { get; set; }
		public string value { get; set; }
		public double epoch { get; set; }
	}
}