# IoT Project

Restful interface to Initial State Buckets

## Getting Started

Two solutions are included for VB.Net and C#. Rest Sharp (106.6.9) is included in both solutions.

## Built, Tested and Facilitated By

* {RestSharp] (https://github.com/restsharp/RestSharp) - Simple Rest and HTTP API Client for .NET
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Fish Code Library](https://www.fishcodelib.com/) - Database tools
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software] (https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **Initial State** - *RC1* - [Initial State](http://www.initialstate.com)
* **David Hary** - *Initial Workarounds* - [ATE Coder](http://www.integratedscientificresources.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md] file at (https://www.bitbucket.org/davidhary/iot/src) for details

## Acknowledgments

* Hat tip to all my mentors
* [Its all a remix] (www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow] (https://www.stackoveflow.com)

## Revision Changes

* Version 1.0.0
	off we go
	

	