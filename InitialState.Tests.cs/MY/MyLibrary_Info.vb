﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Initial State Client Tests"
        Public Const AssemblyDescription As String = "Unit Tests for the Initial State Client Library"
        Public Const AssemblyProduct As String = "isr.Initial.State.Client.Tests.2019"

    End Class

End Namespace
