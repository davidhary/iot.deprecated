﻿Imports InitialStateClient
Namespace InitialStateClientTests
    ''' <summary> Enum extensions tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="3/14/2018" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class BucketTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub


        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " BUCKET CLASSES "

        ''' <summary> A telemetry. </summary>
        ''' <license>
        ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
        ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
        ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
        ''' </license>
        ''' <history date="5/4/2019" by="David" revision=""> Created. </history>
        Friend Class Telemetry
            Public Property Heading As Integer
            Public Property Speed As Double
            Public Property Status As String
        End Class

        ''' <summary> Reads access key. </summary>
        ''' <returns> The access key. </returns>
        Friend Shared Function ReadAccessKey() As String
            Return My.Computer.FileSystem.ReadAllText(My.Settings.AccessKeyFileName)
        End Function

#End Region

#Region " BUCKET TESTS "

        ''' <summary> Handles the exception event. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Error event information. </param>
        Private Sub HandleExceptionEvent(ByVal sender As Object, ByVal e As IO.ErrorEventArgs)
            Assert.IsNotNull(e, $"{e.GetException.ToString}")
        End Sub

        <TestMethod>
        Public Sub StreamTelemetryEventTest()
            Dim accessKey As String = BucketTests.ReadAccessKey
            Try
                ' setup a configuration and read the access key from the private file.
                Dim config As New InitialStateConfig() With {.AccessKey = BucketTests.ReadAccessKey}
                Dim bucketKey As String = "isr_Telemetry"

                ' instantiate an initial state event client and inject a configuration 
                Dim eventClient As New InitialStateEventsClient(config)

                ' create a bucket
                eventClient.CreateBucket(bucketKey, "my telemetry bucket")

                ' create a telemetry event
                Dim telemetry As New Telemetry() With {.Heading = 270, .Speed = 38.29, .Status = "normal"}

                ' Send the event(s)
                eventClient.SendEvents(Of Telemetry)(telemetry, bucketKey, sendAsync:=False)
            Catch ex As InitialStateException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace